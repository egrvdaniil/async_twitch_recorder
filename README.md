# Скрипт для записи стримов с твича

Записывает стримы с Twitch указанного пользователя и загружает их на Яндекс Диск.

## Установка
1. Установите Docker [инструкция](https://docs.docker.com/engine/install/).
2. Установите Docker Compose [инструкция](https://docs.docker.com/compose/install/).
3. Получите OAuth-токен от Яндекс Диска [инструкция](https://yandex.ru/dev/oauth/).
4. Зарегистрируйте новое приложение в Twitch [здесь](https://dev.twitch.tv/console/apps).
5. Получите OAuth-токен от Twitch [здесь](https://twitchapps.com/tmi/).
6. Переименуйте файл `test-params.env` на `.env` и замените значения в файле:
   * `ROOT_PATH` локальный путь, где будут сохраняться стримы перед отправкой в Яндекс Диск.
   * `YANDEX_TOKEN` OAuth-токен от Яндекс Диска, полученный в пункте 3.
   * `CHANNEL` название канала, стримы которого вы хотите записывать.
   * `CLIENT_ID` id приложения Twitch, полученный в пункте 4.
   * `CLIENT_SECRET` секрет приложения Twitch, полученный в пункте 4.
   * `OAUTH_TOKEN` OAuth-токен от аккаунта Twitch, полученный в пункте 5.
   * `USERNAME` имя пользователя вашего Twitch аккаунта.
7. Запустите скрипт коммандой `docker-compose up -d`
