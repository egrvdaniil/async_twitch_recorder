from recorder import record_twitch_stream, video_edit, chat_record
from yandex_disk.async_disk_saver import AsyncDisc
from utils import logger, wait_with_logs, get_params
import asyncio
import os
import datetime
import shutil


class Recorder:
    def __init__(
            self,
            yandex_token: str,
            channels,
            client_id: str,
            client_secret: str,
            oauth_token: str,
            username: str
    ):
        self.channels = channels
        self.yandex_token = yandex_token
        self.interval = 60
        self.quality = "480p"
        self.records_folder = "streams"
        self.client_id = client_id
        self.client_secret = client_secret
        self.oauth_token = oauth_token
        self.username = username
        self.checker = None
        self.chat_width = 40
        self.chat_height = 20
        self.disk_session = None

    def create_folder(self, channel, time):
        path = os.path.join(self.records_folder, channel, time.strftime("%Y-%m-%d_%H-%M-%S"))
        logger(f"creating folder {path}")
        os.makedirs(path)
        return path

    async def send_file_to_disk(self, path_to_file, file_in_disk):
        logger(f"sending file {path_to_file}")
        await self.disk_session.load_file(path_to_file,  file_in_disk)
        logger(f"file sent {path_to_file}")

    async def write_chat_and_send(self, path, start_time):
        # Блокирующий, возможно нужно переделать на async
        logger("inserting chat")
        chat_video_writer = video_edit.ChatVideoWriter(
            start_time = start_time,
            chat_file = os.path.join(path, "chat.txt"),
            in_video_file = os.path.join(path, "record.mp4"),
            out_video_file = os.path.join(path, "record_chat.mp4")
        )
        output = await chat_video_writer.async_create_chat_video()
        logger(output)
        logger("chat inserted")
        logger("sending chat video")
        await self.send_file_to_disk(os.path.join(path, "record_chat.mp4"),
                                     os.path.join(path, "record_chat.mp4"))
        logger("chat video sent")

    async def post_processing(self, path, start_time):
        logger("fixing errors")
        await record_twitch_stream.ffmpeg_copy_and_fix_errors(os.path.join(path, "record_raw.mp4"),
                                                              os.path.join(path, "record.mp4"))
        logger("creating send_record_task task")
        send_record_task = asyncio.create_task(self.send_file_to_disk(os.path.join(path, "record.mp4"), os.path.join(path, "record.mp4")))
        logger("creating send_chat_task task")
        send_chat_task = asyncio.create_task(self.send_file_to_disk(os.path.join(path, "chat.txt"), os.path.join(path, "chat.txt")))
        logger("creating send_record_with_chat_task task")
        send_record_with_chat_task = asyncio.create_task(self.write_chat_and_send(path, start_time))
        await wait_with_logs([send_record_task, send_chat_task, send_record_with_chat_task], f"postprocessing", time=True, delay=15)
        logger(f"deleting folder: {path}")
        shutil.rmtree(path)

    async def start_recording(self, channel, path):
        logger(f"start recording stream {channel}")
        recording_task = asyncio.create_task(record_twitch_stream.start_recording(channel, self.quality, os.path.join(path, "record_raw.mp4")))
        c = chat_record.ChatRecord(
            channel_name=channel,
            oauth_token=self.oauth_token,
            username=self.username,
        )
        logger(f"start recording chat {channel}")
        chat_task = asyncio.create_task(c.run(os.path.join(path, "chat.txt")))

        await wait_with_logs([recording_task], f"Stream recording {channel}", time=True, delay=60)
        logger(f"stopping recording chat {channel}")
        chat_task.cancel()

    async def chek_user(self, channel):
        logger(f"checking channel {channel}")
        async for status in self.checker.check_user(channel):
            logger(f"{channel} status {status}")
            if status:
                start_time = datetime.datetime.now()
                path = self.create_folder(channel, start_time)
                await self.start_recording(channel, path)
                asyncio.create_task(self.post_processing(path, start_time))
            else:
                logger(f"sleeping {self.interval}")
                await asyncio.sleep(self.interval)

    async def run(self):
        self.checker = record_twitch_stream.TwitchCheck(self.client_id, self.client_secret)
        self.disk_session = AsyncDisc(self.yandex_token)
        await self.checker.auth()
        tasks = []
        try:
            for channel in self.channels:
                tasks.append(asyncio.create_task(self.chek_user(channel)))
            await asyncio.wait(tasks)
        finally:
            logger("stopping")
            await self.disk_session.close()
            await self.checker.disconnect()


def start():
    params = get_params()
    r = Recorder(
        yandex_token=params["YANDEX_TOKEN"],
        channels=[params["CHANNEL"]],
        client_id=params["CLIENT_ID"],
        client_secret=params["CLIENT_SECRET"],
        oauth_token=params["OAUTH_TOKEN"],
        username=params["USERNAME"]
    )
    asyncio.run(r.run())


if __name__ == "__main__":
    print("start")
    start()
