import unittest
import yaml
from yandex_disk.async_disk_saver import AsyncDisc
import asyncio
import aiohttp


class TestDiskSaver(unittest.TestCase):
    def setUp(self):
        with open("secret/config.yaml", "r") as f:
            self.config = yaml.safe_load(f)

    async def async_disk_connection(self):
        d = AsyncDisc(self.config["YANDEX_TOKEN"])
        pong = await d.ping()
        self.assertEqual(pong, "pong")
        await d.close()

    async def async_disk_free_space_check(self):
        d = AsyncDisc(self.config["YANDEX_TOKEN"])
        status = await d.check_space(10**100)
        self.assertFalse(status)
        status = await d.check_space(0)
        self.assertTrue(status)
        await d.close()

    async def async_load_file(self):
        d = AsyncDisc(self.config["YANDEX_TOKEN"])
        test_file = "/test.jpg"
        test_folder = "/test1/test2"
        await d.load_file("tests/test_files/test.jpg", test_file)
        await d.delete_file(test_file)
        await d.load_file("tests/test_files/test.jpg", test_folder+test_file)
        await d.delete_file(test_folder.split("/")[1])
        await d.close()

    async def async_create_folder(self):
        d = AsyncDisc(self.config["YANDEX_TOKEN"])
        test_path = "/test1/test2/test3"
        await d.create_path(test_path)
        self.assertTrue(await d.check_folder(test_path))
        await d.delete_file(test_path.split("/")[1])
        await d.close()

    async def async_resource_warning(self):
        for i in range(10):
            async with AsyncDisc(self.config["YANDEX_TOKEN"]) as d:
                for _ in range(5):
                    await d.ping()

    async def async_load_several_files(self):
        d1 = AsyncDisc(self.config["YANDEX_TOKEN"])
        test_folder = "/test"
        task1 = asyncio.create_task(d1.load_file("tests/test_files/record.mp4", test_folder+"/record.mp4"))
        task2 = asyncio.create_task(d1.load_file("tests/test_files/record_chat.mp4", test_folder+"/record_chat.mp4"))
        task3 = asyncio.create_task(d1.load_file("tests/test_files/chat.txt", test_folder+"/chat.txt"))
        await asyncio.wait([task1, task2, task3])
        await d1.delete_file(test_folder)
        await d1.close()

    async def start_all_tests(self):
        print("async_disk_connection")
        await self.async_disk_connection()
        print("async_disk_free_space_check")
        await self.async_disk_free_space_check()
        print("async_create_folder")
        await self.async_create_folder()
        print("async_load_file")
        await self.async_load_file()
        print("async_load_several_files")
        await self.async_load_several_files()

    def test_start(self):
        asyncio.run(self.start_all_tests())



if __name__ == '__main__':
    unittest.main()
