import asyncio
import pprint
from datetime import datetime
from aiohttp import ClientSession, ClientTimeout
import aiofiles
import os
from utils import logger
import random
from contextlib import asynccontextmanager

class DiskConnetionError(Exception):
    def __init__(self, status_code: int, message: str):
        self.status_code = status_code
        self.message = message

    def __str__(self):
        return self.message + ": " + str(self.status_code)


class NotEnoughDiskSpace(Exception):
    def __init__(self, filesize: str):
        self.filesize = filesize

    def __str__(self):
        return f"File too big {self.filesize}"


class AsyncDisc(ClientSession):
    def __init__(self, token, *args, t=None, **kwargs):
        self.yandex_token = token
        self.api_url = "https://cloud-api.yandex.net/v1/disk/"
        self.folder_api_endpoint = "resources"
        self.load_file_endpoint = "resources/upload"
        self.lock = asyncio.Lock()
        if t:
            timeout_param = ClientTimeout(total=t)
        else:
            timeout_param = ClientTimeout(total=0)
        ClientSession.__init__(self,  headers={'Authorization': f"OAuth {self.yandex_token}"}, *args, timeout=timeout_param, **kwargs)


    async def _check_error(self, res):
        if res.ok is not True:
            try:
                data = await res.json()
                message = data["message"] + " " + data["description"]
            except:
                message = "error"
            logger(res.status, message)
            code = res.status
            res.close()
            raise DiskConnetionError(code, message)


    async def _get_disk_data(self):
        async with self.get(self.api_url) as res:
            await self._check_error(res)
            data = await res.json()
            return data

    def print_disk_data(self):
        pprint.pprint(self._get_disk_data())

    async def ping(self):
        logger("start ping")
        await self._get_disk_data()
        logger("Pinged")
        return "pong"

    async def check_space(self, file_size: int) -> bool:
        logger("Checking free space")
        disk_data = await self._get_disk_data()
        free_space = disk_data["total_space"] - disk_data["used_space"]
        logger(f"Free space: {free_space}")
        if file_size >= free_space:
            logger(f"File too big: {file_size}")
            return False
        if file_size >= disk_data["max_file_size"]:
            logger(f"File too big: {file_size}")
            return False
        return True

    async def get_load_url(self, filename: str) -> str:
        params = dict()
        params["path"] = filename
        params["overwrite"] = "true"
        async with self.get(self.api_url + self.load_file_endpoint, params=params) as res:
            await self._check_error(res)
            json_data = await res.json()
            res.close()
            return json_data["href"]

    async def file_sender(self, file_name=None, message=None, delay=60):
        if message:
            file_size = await self.file_size(file_name)
            start_time = datetime.now()
            i = 0
            d = 0
        async with aiofiles.open(file_name, 'rb') as f:
            size = 64 * 1024
            chunk = await f.read(size)
            while chunk:
                yield chunk
                if message:
                    i += 1
                    if d+delay < (sec := (datetime.now() - start_time).seconds):
                        d = sec - (sec % delay)
                        logger(f"{message} | {i*size}/{file_size} bytes | loading time {sec}s")
                chunk = await f.read(size)

    async def file_size(self, file_name=None):
        async with aiofiles.open(file_name, 'rb') as f:
            await f.seek(0, 2)
            filesize = await f.tell()
            return filesize

    # close file on load
    async def load_file(self, path_to_file: str, filename: str):
        head, _ = os.path.split(filename)
        async with self.lock:
            await self.create_path(head)
        filesize = await self.file_size(path_to_file)
        check = await self.check_space(filesize)
        if check is False:
            raise NotEnoughDiskSpace(str(filesize))
        url = await self.get_load_url(filename)
        logger("Loading file")
        async with self.put(url, data=self.file_sender(path_to_file, message=f"loading file {filename}")) as res:
            await self._check_error(res)
        logger(f"File {filename} loaded")

    async def create_path(self, path: str):
        async with self.get(self.api_url + self.folder_api_endpoint, params=dict(path=path)) as res:
            if res.status == 404:
                split_path = path.split("/")
                await self.create_folder("/".join(split_path[:-1]), split_path[-1])
                logger(f"Path {path} created")
            else:
                logger(f"Path {path} exist")

    # Создает папку в с именем folder по адресу path,
    # Если адреса не существует, пытается его создать рекурсивно вызывая себя
    async def create_folder(self, path: str, folder: str):
        exist = await self.check_folder(path)
        if not exist:
            split_path = path.split("/")
            await self.create_folder("/".join(split_path[:-1]), split_path[-1])
        async with self.put(self.api_url + self.folder_api_endpoint, params=dict(path=path + "/" + folder)) as res:
            await self._check_error(res)

    # Проверяет существование папки true - существует, false - не существует
    async def check_folder(self, path: str) -> bool:
        if path == "":
            path = "/"
        async with self.get(self.api_url + self.folder_api_endpoint, params=dict(path=path)) as res:
            if res.status == 404:
                return False
            await self._check_error(res)
        return True

    async def delete_file(self, path):
        async with self.delete(self.api_url + self.folder_api_endpoint, params=dict(path=path)) as res:
            await self._check_error(res)
            logger(f"File {path} deleted")



