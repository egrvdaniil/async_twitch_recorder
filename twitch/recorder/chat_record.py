import asyncio
from utils import logger
import datetime

class ChatRecord:
    def __init__(self, channel_name, oauth_token, username):
        self.channel_name = channel_name
        self.oauth = oauth_token
        self.username = username
        self.reader = None
        self.writer = None

    def _write_command(self, command):
        self.writer.write(command.encode() + b'\r\n')

    async def _auth(self):
        self._write_command(f"PASS {self.oauth}")
        self._write_command(f"NICK {self.username}")
        welcome_messages = [
            f':tmi.twitch.tv 001 {self.username} :Welcome, GLHF!\r\n',
            f':tmi.twitch.tv 002 {self.username} :Your host is tmi.twitch.tv\r\n',
            f":tmi.twitch.tv 003 {self.username} :This server is rather new\r\n",
            f":tmi.twitch.tv 004 {self.username} :-\r\n",
            f":tmi.twitch.tv 375 {self.username} :-\r\n",
            f":tmi.twitch.tv 372 {self.username} :You are in a maze of twisty passages, all alike.\r\n",
            f":tmi.twitch.tv 376 {self.username} :>\r\n",
        ]
        for message in welcome_messages:
            chat_message = ""
            try:
                chat_message = await asyncio.wait_for(self.reader.readline(), 10)
            except asyncio.TimeoutError:
                assert False, "can't connect"
            if message.encode() != chat_message:
                assert False, repr(message)+"!="+repr(chat_message)
        logger("authenticated")

    async def _join_chat(self):
        self._write_command(f"JOIN #{self.channel_name}")
        chat_join_welcome = [
            f":{self.username}!{self.username}@{self.username}.tmi.twitch.tv JOIN #{self.channel_name}\r\n",
            f":{self.username}.tmi.twitch.tv 353 {self.username} = #{self.channel_name} :{self.username}\r\n",
            f":{self.username}.tmi.twitch.tv 366 {self.username} #{self.channel_name} :End of /NAMES list\r\n",
        ]
        for message in chat_join_welcome:
            chat_message = ""
            try:
                chat_message = await asyncio.wait_for(self.reader.readline(), 10)
            except asyncio.TimeoutError:
                assert False
            if message.encode() != chat_message:
                assert False, repr(message)+"!="+repr(chat_message)
        logger("Chat connected")

    async def connect(self):
        logger("Connecting to chat")
        self.reader, self.writer = await asyncio.open_connection('irc.chat.twitch.tv', 6667)
        await self._auth()
        await self._join_chat()

    async def disconnect(self):
        self.writer.close()
        await self.writer.wait_closed()
        logger("disconnected")

    def _split_message(self, message: str):
        message_text = message.split(f"#{self.channel_name} :")[1]
        message_user = message.split(" ", 1)[0].split("!", 1)[0][1:]
        return dict(user=message_user, text=message_text)

    async def get_message(self):
        while data := await self.reader.readline():
            message = data.decode().replace("\r\n", "")
            if message == "PING :tmi.twitch.tv":
                self._write_command("PONG :tmi.twitch.tv")
            if "PRIVMSG" in message:
                yield self._split_message(message)

    async def write_chat(self, file_name):
        with open(file_name, "w") as chat_file:
            async for message in self.get_message():
                logger(message["user"]+": "+message["text"])
                time = datetime.datetime.now()
                chat_file.write(time.isoformat() + "@" + message["user"] + ": " + message["text"] + "\r\n")

    async def run(self, filename):
        try:
            await self.connect()
            await self.write_chat(filename)
        finally:
            await self.disconnect()

