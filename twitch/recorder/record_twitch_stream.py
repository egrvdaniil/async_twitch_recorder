from aiohttp import ClientSession
from aiohttp import ClientResponseError
from utils import logger
import asyncio
import os


class TwitchCheck:
    def __init__(self, client_id, client_secret):
        self.client_id = client_id
        self.client_secret = client_secret
        self.token_url = "https://id.twitch.tv/oauth2/token?client_id=" + self.client_id + "&client_secret=" \
                         + self.client_secret + "&grant_type=client_credentials"
        self.url = "https://api.twitch.tv/helix/streams"
        self.access_token = None
        self.session = None

    async def auth(self):
        self.access_token = await self.fetch_access_token()
        self.session = ClientSession(
            headers={"Client-ID": self.client_id, "Authorization": "Bearer " + self.access_token})

    async def disconnect(self):
        await self.session.close()

    async def fetch_access_token(self):
        async with ClientSession() as session:
            async with session.post(self.token_url, timeout=15) as token_response:
                token_response.raise_for_status()
                token = await token_response.json()
                return token["access_token"]

    async def check_user(self, twitch_channel):
        info = None
        while True:
            try:
                async with self.session.get(self.url + "?user_login=" + twitch_channel, timeout=15) as r:
                    r.raise_for_status()
                    info = await r.json()
                    if info is None or not info["data"]:
                        yield False
                    else:
                        yield True
            except ClientResponseError as e:
                logger(e.args)


async def start_recording(twitch_channel, quality, filename):
    proc = await asyncio.create_subprocess_shell("streamlink --twitch-disable-ads twitch.tv/"
                                                 + twitch_channel
                                                 + " "
                                                 + quality
                                                 + " -o "
                                                 + filename,
                                                 stdout=asyncio.subprocess.PIPE,
                                                 stderr=asyncio.subprocess.PIPE
                                                 )
    stdout, stderr = await proc.communicate()
    logger("recording stream is done, processing video file")
    logger(f'Exited with {proc.returncode}]')
    if stdout:
        logger(f'[stdout]\n{stdout.decode()}')
    if stderr:
        logger(f'[stderr]\n{stderr.decode()}')



async def ffmpeg_copy_and_fix_errors(rec_file, out_file):
    proc = await asyncio.create_subprocess_shell(f"ffmpeg -err_detect ignore_err -i {rec_file} -c copy {out_file}",
                                                 stdout=asyncio.subprocess.PIPE,
                                                 stderr=asyncio.subprocess.PIPE
                                                 )

    stdout, stderr = await proc.communicate()
    os.remove(rec_file)
    logger("fixing file")
    logger(f'Exited with {proc.returncode}]')
    if stdout:
        logger(f'[stdout]\n{stdout.decode()}')
    if stderr:
        logger(f'[stderr]\n{stderr.decode()}')



