import functools
import asyncio
import datetime
import io
from utils import RemoveControlChars
from moviepy.video.tools.subtitles import SubtitlesClip
from moviepy.editor import VideoFileClip, TextClip, CompositeVideoClip, VideoClip
from contextlib import redirect_stdout, redirect_stderr


class ChatLines:
    def __init__(self, chat_width: int, chat_height: int):
        self.chat_width = chat_width
        self.chat_height = chat_height
        self.chat_lines=[]
        self._new_chat_lines()
        self.remover = RemoveControlChars()

    def _new_chat_lines(self):
        for i in range(self.chat_height):
            self.chat_lines.append("\u00a0"*self.chat_width)

    def _split_to_lines(self, message):
        lines = []
        message = message.strip()
        while message:
            lines.append(message[:self.chat_width].ljust(self.chat_width, "\u00a0"))
            message = message[self.chat_width:]
        return lines[::-1]

    def add_message(self, user_name: str, message: str):
        message = self.remover.remove(message)
        chat_message = user_name+": "+message
        chat_message_lines = self._split_to_lines(chat_message)
        self.chat_lines = (chat_message_lines + self.chat_lines)[:self.chat_height]

    def get_chat_lines(self):
        return self.chat_lines[::-1]


def split_line(line: str):
    time_split = line.split("@", 1)
    time = datetime.datetime.strptime(time_split[0], "%Y-%m-%dT%H:%M:%S.%f")
    user_name = time_split[1].split(": ", 1)[0]
    message = time_split[1].split(": ", 1)[1]

    return dict(time=time, user_name=user_name, message=message)


def run_in_executor(f):
    @functools.wraps(f)
    def inner(*args, **kwargs):
        loop = asyncio.get_running_loop()
        return loop.run_in_executor(None, lambda: f(*args, **kwargs))
    return inner


class ChatVideoWriter:
    def __init__(self,start_time: datetime.datetime, chat_file, in_video_file, out_video_file, chat_width=40, chat_height=20):
        self.chat_lines = ChatLines(chat_width, chat_height)
        self.start_time = start_time
        self.chat_file = chat_file
        self.in_video_file = in_video_file
        self.out_video_file = out_video_file
        self.iteration = 0
        self.working_text = ""

    def generate_chat_text(self):
        chat_all_text = list()
        i = 0
        with open(self.chat_file) as f_chat:
            time_part_start = self.start_time
            time_part_end = self.start_time + datetime.timedelta(seconds=3)
            while line := f_chat.readline():
                chat_message = split_line(line)
                while True:
                    if time_part_start <= chat_message["time"] < time_part_end:
                        self.chat_lines.add_message(chat_message["user_name"], chat_message["message"])
                        break
                    i += 1
                    sec = (time_part_start - self.start_time).seconds
                    chat_time = [sec, sec+3]
                    chat_text = "\n".join(self.chat_lines.get_chat_lines())
                    time_part_start = time_part_end
                    time_part_end = time_part_start + datetime.timedelta(seconds=3)
                    chat_all_text.append((chat_time, chat_text))
            return chat_all_text

    def clip_generator(self, txt):
        self.iteration += 1
        self.working_text = txt
        print(self.iteration)
        text_clip = TextClip(txt, fontsize=10, color='white', font="Noto-Mono")
        text_clip = text_clip.on_color(size=(text_clip.w, text_clip.h), color=(0, 0, 0), col_opacity=0.6)
        return text_clip

    def create_chat_video(self):
        try:
            clip = VideoFileClip(self.in_video_file)
            sub = SubtitlesClip(self.generate_chat_text(), self.clip_generator)
            video = CompositeVideoClip([clip, sub])
            video.write_videofile(self.out_video_file)
            self.iteration = 0
            self.working_text = ""
        except Exception as exc:
            print(f"error | iteration {self.iteration} | text {self.working_text}")
            raise exc

    @run_in_executor
    def async_create_chat_video(self):
        f_out = io.StringIO()
        f_err = io.StringIO()
        with redirect_stderr(f_err):
            with redirect_stdout(f_out):
                self.create_chat_video()
        return f_out.getvalue()+f_err.getvalue()
