import asyncio
from typing import List
from datetime import datetime
import os
import yaml
import re, itertools


def logger(*args, level="info"):
    if level == "info":
        print(datetime.now().isoformat(sep=" "), "|  ", *args)


async def wait_with_logs(tasks: List[asyncio.Task], message, time: bool = False, delay: int = 15):
    i = 0
    successful = [task.done() for task in tasks]
    start_time = datetime.now()
    while True:
        if i > delay:
            if time:
                working_time = (datetime.now() - start_time).seconds
                logger(message+f" | Task working {working_time}s")
            else:
                logger(message)

            i = 0
        else:
            i += 1
        for (n, task) in enumerate(tasks):
            successful[n] = task.done()
            try:
                status = task.exception()
                if status is not None:
                    logger(message+f" | {task.get_name()} end with error {str(task.exception())}")
                    raise task.exception()
            except asyncio.exceptions.InvalidStateError:
                pass
        if sum(successful) == len(tasks):
            return
        await asyncio.sleep(1)


def get_params():
    try:
        with open("secret/config.yaml", "r") as f:
            params = yaml.safe_load(f.read())
    except FileNotFoundError:
        params = dict()
        params["YANDEX_TOKEN"] = os.environ["YANDEX_TOKEN"]
        params["CHANNEL"] = os.environ["CHANNEL"]
        params["CLIENT_ID"] = os.environ["CLIENT_ID"]
        params["CLIENT_SECRET"] = os.environ["CLIENT_SECRET"]
        params["OAUTH_TOKEN"] = os.environ["OAUTH_TOKEN"]
        params["USERNAME"] = os.environ["USERNAME"]
    return params


class RemoveControlChars:
    def __init__(self):
        control_chars = ''.join(map(chr, itertools.chain(range(0x00,0x20), range(0x7f,0xa0))))
        self.control_char_re = re.compile('[%s]' % re.escape(control_chars))

    def remove(self, s):
        return self.control_char_re.sub('', s)

